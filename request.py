from datetime import datetime, date, time
import requests
import json
import psycopg2
import secret_data


def station_code(s1, s2):

    conn = psycopg2.connect("dbname=postgres user=postgres password=postgres")
    cursor = conn.cursor()
    cursor.execute(f'SELECT * FROM MY_DATABASE.esr WHERE name = {s1}')
    records = cursor.fetchone()
    if records is None:
        return None, None
    else:
        from_code = records[1]
    cursor.execute(f'SELECT * FROM MY_DATABASE.esr WHERE name = {s2}')
    records = cursor.fetchone()
    if records is None:
        return None, None
    else:
        to_code = records[1]
    cursor.close()
    conn.close()
    return from_code, to_code


def get_request(s1, s2):

    from_code, to_code = station_code(s1.lower(), s2.lower())
    if from_code is None:
        return None
    today = datetime.now().date()
    now = datetime.now().time()
    url = f"https://api.rasp.yandex.net/v3.0/search/?apikey={secret_data.key}&from={str(from_code)}&to={str(to_code)}&" + \
          f"date={today}&transport_types=suburban&system=esr"
    res = requests.get(url)
    data = json.loads(res.text)
    for current in data['segments']:
        year = current['departure'][0:4]
        month = current['departure'][5:7]
        day = current['departure'][8:10]
        hour = current['departure'][11:13]
        minute = current['departure'][14:16]
        d = date(int(year), int(month), int(day))
        t = time(int(hour), int(minute))
        res = []
        if t > now or d > today:
            res.append(current['departure'][11:16])
            res.append(current['arrival'][11:16])
            res.append(current['stops'])
            res.append(current['tickets_info']['places'][0]['price']['whole'])
            return res
